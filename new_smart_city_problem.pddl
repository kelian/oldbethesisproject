(define (problem new_smart_city_problem)
    (:domain new_smart_city_domain)

    (:objects
        play_HP_Soundtrack - action_play_HP_Soundtrack
        feed_buckbeak - action_feed_buckbeak
        Hagrid_s_house The_Great_Lake The_Room_of_Requirement Tartu_Bus_Station - location
        pet_fang - action_pet_fang
        swim_with_mermaids - action_swim_with_mermaids
    )

    (:init
        (action_at feed_buckbeak Hagrid_s_house)
        (not (action_is_executed feed_buckbeak))
        (has_actions)
        (action_at pet_fang Hagrid_s_house)
        (not (action_is_executed pet_fang))
        (action_at swim_with_mermaids The_Great_Lake)
        (not (action_is_executed swim_with_mermaids))
        (action_at play_HP_Soundtrack The_Room_of_Requirement)
        (not (action_is_executed play_HP_Soundtrack))
        (at Tartu_Bus_Station)
        (not (action_in_progress))
    )

    (:goal
        (action_is_executed pet_fang)
    )

)