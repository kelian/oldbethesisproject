import java.io.*;
import java.sql.Timestamp;

public class Main {

    private static String pddlDomainFile = "new_smart_city_domain";
    private static String pddlProblemFile = "new_smart_city_problem";

    public static void main(String[] args) throws Exception {
//        Timestamp startTimeStamp = new Timestamp(System.currentTimeMillis());
//        System.out.println("Start: " + startTimeStamp.toString());

        runSmartCity();

        // runBlocksWorld(); // another example for PDDL Generator

//        Timestamp endTimeStamp = new Timestamp(System.currentTimeMillis());
//        System.out.println("End: " + endTimeStamp.toString());
//
//        System.out.println("Difference: " + (endTimeStamp.getTime() - startTimeStamp.getTime()));

    }

    private static void runBlocksWorld() throws Exception {
        new PDDLGenerator(
                "database_response_blocks.json",
                "blocks_domain",
                "blocks_problem");
    }

    private static void runSmartCity() throws Exception {
        runPDDLGenerator();
        //runPlanner();
        //runBPMNGenerator();
    }

    private static void runBPMNGenerator() throws Exception {
        String plannerOutput =
                ";new_smart_city_problem\n" +
                        "1:(start_action_task)\n" +
                        "1:(move_to tartu_bus_station hagrid_house_3)\n" +
                        "2:(feed_buckbeak_3 feed_buckbeak_3 hagrid_house_3)\n" +
                        "3:(set_location_done_1 hagrid_house_3 feed_buckbeak_3)\n" +
                        "3:(start_action_task)\n" +
                        "3:(move_to hagrid_house_3 hagrid_house_2)\n" +
                        "4:(set_location_goal_done goal_3 hagrid_house_3)\n" +
                        "4:(feed_buckbeak_2 feed_buckbeak_2 hagrid_house_2)\n" +
                        "5:(set_location_done_1 hagrid_house_2 feed_buckbeak_2)\n" +
                        "5:(start_action_task)\n" +
                        "5:(move_to hagrid_house_2 hagrid_house)\n" +
                        "6:(set_location_goal_done goal_2 hagrid_house_2)\n" +
                        "6:(feed_buckbeak feed_buckbeak hagrid_house)\n" +
                        "7:(set_location_done_1 hagrid_house feed_buckbeak)\n" +
                        "8:(set_location_goal_done goal_1 hagrid_house)\n" +
                        "Time: 320";

        new BPMNGenerator(plannerOutput);
    }

    private static void runPDDLGenerator() throws Exception {
        new PDDLGenerator(
                "database_response.json",
                pddlDomainFile,
                pddlProblemFile);
    }

    private static void runPlanner() throws IOException, InterruptedException {
        Runtime rt = Runtime.getRuntime();

        String domainFile = pddlDomainFile + ".pddl";
        String problemFile = pddlProblemFile + ".pddl";
        String executionCommand = "./planner/vhpop " + domainFile + " " + problemFile;
        System.out.println(executionCommand);

        runCommand(rt, executionCommand);

        // Run docker image, write to file, read the file, rm command
    }

    private static void runCommand(Runtime rt, String command) throws IOException, InterruptedException {
        Process pr = rt.exec(command);

        // fat jar

        if (pr.waitFor() != 0) {
            printOutInputStream(pr.getErrorStream());
        } else {
            System.out.println("SUCCESS");
        }
    }

    private static void printOutInputStream(InputStream is) {
        BufferedReader br = null;

        try {
            br = new BufferedReader(new InputStreamReader(is));

            String line;

            while ((line = br.readLine()) != null) {
                if (line.equalsIgnoreCase("quit")) {
                    break;
                }
                System.out.println(line);
            }

        }
        catch (IOException ioe) {
            System.out.println("Exception while reading input " + ioe);
        }
        finally {
            try {
                if (br != null) {
                    br.close();
                }
            }
            catch (IOException ioe) {
                System.out.println("Error while closing stream: " + ioe);
            }
        }
    }

}
