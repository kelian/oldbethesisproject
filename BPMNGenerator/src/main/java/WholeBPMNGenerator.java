import model.bpmn.BPMN;
import model.bpmn.BPMNProcess;
import model.bpmn.BPMNTask;
import model.bpmn.SequenceFlow;
import model.bpmn.diagram.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class WholeBPMNGenerator {

    private BPMN wholeBPMN = new BPMN();
    private BPMN baseBPMN;
    private Map<Integer, List<BPMN>> planBPMNList;

    private int wholeBPMNLastId = 4; // Because startEvent is 2 and endEvent is 3.

    private Bounds wholeBPMNLastBounds;
    private Bounds currentLastBounds;

    private String wholeLastElement;
    private String baseEndElement;

    float currentMaxY = 0f;

    WholeBPMNGenerator(BPMNInterface bpmnInterface, BPMN baseBPMN, Map<Integer, List<BPMN>> planBPMNList) throws Exception {
        this.baseBPMN = baseBPMN;
        this.planBPMNList = planBPMNList;

        generateWholeBPMN(bpmnInterface);
    }

    private void generateWholeBPMN(BPMNInterface bpmnInterface) throws Exception {
        wholeBPMN.setDefinitions(baseBPMN.getDefinitions());
        generateBody();

        bpmnInterface.setWholeBPMN(wholeBPMN);
    }

    private void generateBody() {
        generateProcess();
        generateDiagram();

        addStartEventToDiagram();

        addItems();
    }

    private void generateProcess() {
        BPMNProcess wholeProcess = new BPMNProcess(baseBPMN.getProcess().getAttributes());
        wholeProcess.setStartEvent(baseBPMN.getProcess().getStartEvent());
        wholeProcess.setEndEvent(baseBPMN.getProcess().getEndEvent());

        wholeLastElement = wholeProcess.getStartEvent().getId();
        baseEndElement = wholeProcess.getEndEvent().getId();

        wholeBPMN.setProcess(wholeProcess);
    }

    private void generateDiagram() {
        BPMNDiagram baseDiagram = baseBPMN.getDiagram();
        BPMNDiagram diagram = new BPMNDiagram(baseDiagram.getDocumentation(), baseDiagram.getId(), baseDiagram.getName());
        wholeBPMN.setDiagram(diagram);

        generatePlane();
    }

    private void generatePlane() {
        BPMNPlane basePlane = baseBPMN.getDiagram().getBpmnPlane();
        BPMNPlane plane = new BPMNPlane(basePlane.getBpmnElement());

        wholeBPMN.getDiagram().setBpmnPlane(plane);
    }

    private void addStartEventToDiagram() {
        BPMNShape baseShape = baseBPMN.getDiagram().getBpmnPlane().getBpmnShapeList().get(0);
        wholeBPMN.getDiagram().getBpmnPlane().addBpmnShape(baseShape);

        wholeBPMNLastBounds = baseShape.getBounds();
        currentLastBounds = wholeBPMNLastBounds;
    }

    private void addItems() {
        for (Integer time : planBPMNList.keySet()) {
            List<BPMN> bpmnList = planBPMNList.get(time);

            if (bpmnList.size() == 1) {
                addBPMNItems(bpmnList.get(0), 0f);
            } else {
                addParallelItems(bpmnList);
            }
        }

        addEndSequenceFlow();
        addEndShape();
        addEdges();
    }

    private void addParallelItems(List<BPMN> bpmnList) {
        String parallelGatewayId = "_" + wholeBPMNLastId;
        wholeBPMNLastId += 1;

        addParallelGateway(parallelGatewayId);

        float extraY = 0f;
        List<String> lastElementsList = new ArrayList<>();

        for (BPMN bpmn : bpmnList) {
            wholeLastElement = parallelGatewayId;
            addBPMNItems(bpmn, extraY);

            extraY = currentMaxY;
            lastElementsList.add(wholeLastElement);
        }

        addLastParallelGateway(lastElementsList);
    }

    private void addParallelGateway(String parallelGatewayId) {
        addParallelTask(parallelGatewayId);
        addParallelSequenceFlow(parallelGatewayId, wholeLastElement);
        addParallelShape(parallelGatewayId);
    }

    private void addLastParallelGateway(List<String> lastElementsList) {
        String parallelGatewayId = "_" + wholeBPMNLastId;
        wholeBPMNLastId += 1;

        addParallelTask(parallelGatewayId);
        addParallelShape(parallelGatewayId);

        for (String id : lastElementsList) {
            addParallelSequenceFlow(parallelGatewayId, id);
        }
    }

    private void addParallelTask(String parallelGatewayId) {
        BPMNTask parallelTask = new BPMNTask("parallelGateway");
        parallelTask.addAttribute("name", "ParallelGateway");
        parallelTask.setId(parallelGatewayId);

        wholeBPMN.getProcess().addBpmnTask(parallelTask);
    }

    private void addParallelSequenceFlow(String parallelGatewayId, String sourceRef) {
        SequenceFlow sequenceFlow = new SequenceFlow();
        sequenceFlow.setId("_" + wholeBPMNLastId);
        wholeBPMNLastId += 1;

        sequenceFlow.setTargetRef(parallelGatewayId);
        sequenceFlow.setSourceRef(sourceRef);

        wholeLastElement = parallelGatewayId;

        wholeBPMN.getProcess().addSequenceFlow(sequenceFlow);
    }

    private void addParallelShape(String parallelGatewayId) {
        BPMNShape shape = new BPMNShape();

        shape.setBpmnElement(parallelGatewayId);
        shape.setId("Shape-" + parallelGatewayId);

        Bounds bounds = new Bounds();
        bounds.setHeight("32.0");
        bounds.setWidth("32.0");

        bounds.setX(wholeBPMNLastBounds.getXFloat() + wholeBPMNLastBounds.getWidthFloat() + 100);
        bounds.setY(wholeBPMNLastBounds.getY());

        wholeBPMNLastBounds = bounds;

        shape.setBounds(bounds);

        shape.setBpmnLabel(new BPMNLabel());
        shape.getBpmnLabel().setBounds(new Bounds("32.0", "32.0", "0", "0"));

        wholeBPMN.getDiagram().getBpmnPlane().addBpmnShape(shape);
    }

    private void addBPMNItems(BPMN bpmn, float extraY) {
        String currentStartElement = bpmn.getProcess().getStartEvent().getId();
        String currentEndElement = bpmn.getProcess().getEndEvent().getId();

        int taskExtraId = wholeBPMNLastId - getMinimumTaskId(bpmn.getProcess().getBpmnTasks());
        addTask(bpmn.getProcess().getBpmnTasks(), taskExtraId);

        int sequenceExtraId = wholeBPMNLastId - getMinimumSequenceId(bpmn.getProcess().getSequenceFlows());
        addSequenceFlows(bpmn.getProcess(), taskExtraId, sequenceExtraId, currentStartElement, currentEndElement);

        addElements(bpmn, extraY, taskExtraId, currentStartElement, currentEndElement);

        wholeBPMNLastBounds = currentLastBounds;
    }

    private void addTask(List<BPMNTask> bpmnTasks, int taskExtraId) {
        for (BPMNTask task : bpmnTasks) {
            task.setId(getNewId(task.getId(), taskExtraId));

            wholeBPMN.getProcess().addBpmnTask(task);
        }
    }

    private void addSequenceFlows(BPMNProcess process, int taskExtraId, int sequenceExtraId, String currentStartElement,
                                  String currentEndElement) {

        for (SequenceFlow sequenceFlow : process.getSequenceFlows()) {
            sequenceFlow.setId(getNewId(sequenceFlow.getId(), sequenceExtraId));
            addSequenceSourceRef(sequenceFlow, currentStartElement, taskExtraId);

            if (!sequenceFlow.getTargetRef().equals(currentEndElement)) {
                addSequenceTargetRef(sequenceFlow, taskExtraId);
                wholeBPMN.getProcess().addSequenceFlow(sequenceFlow);
            } else {
                wholeLastElement = sequenceFlow.getSourceRef();
            }
        }
    }

    private void addSequenceSourceRef(SequenceFlow sequenceFlow, String currentStartElement, int taskExtraId) {
        if (sequenceFlow.getSourceRef().equals(currentStartElement)) {
            sequenceFlow.setSourceRef(wholeLastElement);

        } else {
            String newSourceId = getNewId(sequenceFlow.getSourceRef(), taskExtraId);
            sequenceFlow.setSourceRef(newSourceId);
        }
    }

    private void addSequenceTargetRef(SequenceFlow sequenceFlow, int taskExtraId) {
        String newTargetId = getNewId(sequenceFlow.getTargetRef(), taskExtraId);
        sequenceFlow.setTargetRef(newTargetId);
    }

    private void addElements(BPMN bpmn, float extraY, int taskExtraId, String currentStartElement, String currentEndElement) {
        for (BPMNShape shape : bpmn.getDiagram().getBpmnPlane().getBpmnShapeList()) {
            if (notStartOrLastElement(shape.getBpmnElement(), currentStartElement, currentEndElement)) {
                float extraValue = wholeBPMNLastBounds.getXFloat() + 5; // extra space;

                String newElement = getNewId(shape.getBpmnElement(), taskExtraId);
                String newId = "Shape-" + newElement;

                shape.setBpmnElement(newElement);
                shape.setId(newId);

                float currentX = Float.parseFloat(shape.getBounds().getX());
                float newX = currentX + extraValue;
                shape.getBounds().setX(String.valueOf(newX));
                shape.getBounds().setY(shape.getBounds().getYFloat() + extraY);

                wholeBPMN.getDiagram().getBpmnPlane().addBpmnShape(shape);

                if (shape.getBounds().getXFloat() > currentLastBounds.getXFloat()) {
                    currentLastBounds = shape.getBounds();
                }

                if (shape.getBounds().getYFloat() > currentMaxY) {
                    currentMaxY = shape.getBounds().getYFloat();
                }
            }
        }
    }

    private void addEdges() {
        for (SequenceFlow sequenceFlow : wholeBPMN.getProcess().getSequenceFlows()) {
            BPMNEdge edge = new BPMNEdge();
            edge.setBpmnElement(sequenceFlow.getId());
            edge.setId("BPMNEdge_" + sequenceFlow.getId());
            edge.setSourceElement(sequenceFlow.getSourceRef());
            edge.setTargetElement(sequenceFlow.getTargetRef());

            setStartWayPoint(edge, edge.getSourceElement());
            setEndWayPoint(edge, edge.getTargetElement());

            edge.setBpmnLabel(baseBPMN.getDiagram().getBpmnPlane().getBpmnEdgeList().get(0).getBpmnLabel());

            wholeBPMN.getDiagram().getBpmnPlane().addBPMNEdge(edge);
        }
    }

    private void setStartWayPoint(BPMNEdge edge, String sourceElement) {
        BPMNShape shape = getShapeById(sourceElement);

        if (shape != null) {
            Waypoint waypoint = new Waypoint();

            Float x = shape.getBounds().getWidthFloat() + shape.getBounds().getXFloat();
            Float y = (shape.getBounds().getHeightFloat() / 2) + shape.getBounds().getYFloat();

            waypoint.setX(String.valueOf(x));
            waypoint.setY(String.valueOf(y));

            edge.addWaypoint(waypoint);
        }
    }

    private void setEndWayPoint(BPMNEdge edge, String targetElement) {
        BPMNShape shape = getShapeById(targetElement);

        if (shape != null) {
            Waypoint waypoint = new Waypoint();

            Float x = shape.getBounds().getXFloat();
            Float y = (shape.getBounds().getHeightFloat() / 2) + shape.getBounds().getYFloat();

            waypoint.setX(String.valueOf(x));
            waypoint.setY(String.valueOf(y));

            edge.addWaypoint(waypoint);
        }
    }

    private BPMNShape getShapeById(String sourceElement) {
        for (BPMNShape shape : wholeBPMN.getDiagram().getBpmnPlane().getBpmnShapeList()) {
            if (shape.getBpmnElement().equals(sourceElement)) {
                return shape;
            }
        }
        return null;
    }

    private void addEndSequenceFlow() {
        SequenceFlow sequenceFlow = new SequenceFlow();
        sequenceFlow.setId("_" + (wholeBPMNLastId + 1));

        sequenceFlow.setSourceRef(wholeLastElement);
        sequenceFlow.setTargetRef(baseEndElement);

        wholeBPMN.getProcess().addSequenceFlow(sequenceFlow);
    }

    private void addEndShape() {
        float x = wholeBPMNLastBounds.getXFloat() + wholeBPMNLastBounds.getWidthFloat() + 100;

        BPMNShape baseLastShape = baseBPMN.getDiagram().getBpmnPlane().getBpmnShapeList().get(1);
        baseLastShape.getBounds().setX(x);

        wholeBPMN.getDiagram().getBpmnPlane().addBpmnShape(baseLastShape);
    }

    private int getMinimumTaskId(List<BPMNTask> bpmnTasks) {
        int minimumId = getId(bpmnTasks.get(0).getId());

        for (BPMNTask task : bpmnTasks) {
            int id = getId(task.getId());

            if (id < minimumId) {
                minimumId = id;
            }
        }

        return minimumId;
    }

    private int getMinimumSequenceId(List<SequenceFlow> sequenceFlows) {
        int minimumId = getId(sequenceFlows.get(0).getId());

        for (SequenceFlow sequenceFlow : sequenceFlows) {
            int id = getId(sequenceFlow.getId());

            if (id < minimumId) {
                minimumId = id;
            }
        }

        return minimumId;
    }

    private int getId(String id) {
        return Integer.parseInt(id.substring(1));
    }

    private String getNewId(String id, int extraId) {
        int idNr = getId(id) + extraId;

        if ((idNr + 1) > wholeBPMNLastId) {
            wholeBPMNLastId = (idNr + 1);
        }

        return "_" + idNr;
    }

    private boolean notStartOrLastElement(String id, String currentStartElement, String currentEndElement) {
        return !id.equals(currentStartElement) && !id.equals(currentEndElement);
    }
}
