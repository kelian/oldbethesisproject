import model.bpmn.*;
import model.bpmn.diagram.*;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import static javax.xml.stream.XMLStreamConstants.END_ELEMENT;
import static javax.xml.stream.XMLStreamConstants.START_ELEMENT;
import static utils.BPMNUtils.deleteFile;
import static utils.BPMNUtils.writeFile;

class BPMNParser {

    BPMNParser() { }

    BPMN getBpmnData(String bpmnText) throws Exception {
        BPMN bpmn = new BPMN();

        String fileName = "bpmn.xml";
        writeFile(fileName, bpmnText);

        try (FileInputStream fis = new FileInputStream("bpmn.xml")) {
            XMLInputFactory xmlInFact = XMLInputFactory.newInstance();
            XMLStreamReader reader = xmlInFact.createXMLStreamReader(fis);

            while(reader.hasNext()) {
                int eventType = reader.next();
                switch (eventType) {
                    case START_ELEMENT: {
                        if (reader.getLocalName().equals("definitions")) {
                            parseDefinitions(bpmn, reader);
                        } else if (reader.getLocalName().equals("process")) {
                            parseProcessBlock(bpmn, reader);
                        } else if (reader.getLocalName().equals("BPMNDiagram")) {
                            parseDiagramBlock(bpmn, reader);
                        }
                        break;
                    }
                    case END_ELEMENT:
                        break;

                }
            }
        }

        deleteFile(fileName);

        return bpmn;
    }

    private void parseDefinitions(BPMN bpmn, XMLStreamReader reader) {
        BPMNDefinitions definitions = new BPMNDefinitions();

        for (int i = 0; i < reader.getAttributeCount(); i++) {
            String attributeName = reader.getAttributeLocalName(i);
            definitions.addAttribute(attributeName, reader.getAttributeValue(i));
        }

        for (int i = 0; i < reader.getNamespaceCount(); i++) {
            definitions.addNameSpace(reader.getNamespacePrefix(i), reader.getNamespaceURI(i));
        }

        bpmn.setDefinitions(definitions);
    }

    private void parseProcessBlock(BPMN bpmn, XMLStreamReader reader) throws XMLStreamException {
        BPMNProcess process = parseProcess(reader);

        List<BPMNTask> bpmnTasks = new ArrayList<>();
        List<SequenceFlow> sequenceFlows = new ArrayList<>();

        while(reader.hasNext()) {
            int eventType = reader.next();
            switch (eventType) {
                case START_ELEMENT: {
                    switch (reader.getLocalName()) {
                        case "sequenceFlow":
                            parseSequenceFlows(sequenceFlows, reader);
                            reader.next();
                            break;
                        case "startEvent":
                            process.setStartEvent(parseTasksAndEvents(reader));
                            reader.next();
                            break;
                        case "endEvent":
                            process.setEndEvent(parseTasksAndEvents(reader));
                            reader.next();
                            break;
                        default:
                            bpmnTasks.add(parseTasksAndEvents(reader));
                            reader.next();
                            break;
                    }
                    break;
                }
                case END_ELEMENT: {
                    process.setBpmnTasks(bpmnTasks);
                    process.setSequenceFlows(sequenceFlows);
                    bpmn.setProcess(process);
                    return;
                }
            }
        }
    }

    private BPMNProcess parseProcess(XMLStreamReader reader) {
        BPMNProcess process = new BPMNProcess();

        for (int i = 0; i < reader.getAttributeCount(); i++) {
            String attributeName = reader.getAttributeLocalName(i);
            process.addAttribute(getAttributeName(reader, attributeName, i), reader.getAttributeValue(i));
        }

        return process;
    }

    private BPMNTask parseTasksAndEvents(XMLStreamReader reader) {
        BPMNTask task = new BPMNTask(reader.getLocalName());
        for (int i = 0; i < reader.getAttributeCount(); i++) {
            String attributeName = reader.getAttributeLocalName(i);
            if ("id".equals(attributeName)) {
                task.setId(reader.getAttributeValue(i));
            } else {
                task.addAttribute(getAttributeName(reader, attributeName, i), reader.getAttributeValue(i));
            }
        }

        return task;
    }

    private String getAttributeName(XMLStreamReader reader, String attributeName, int i) {
        String prefix = reader.getAttributePrefix(i);
        if (prefix != null && !prefix.isEmpty()) {
            return prefix + ":" + attributeName;
        }
        return attributeName;
    }

    private void parseSequenceFlows(List<SequenceFlow> sequenceFlows, XMLStreamReader reader) {
        SequenceFlow sequenceFlow = new SequenceFlow();

        for (int i = 0; i < reader.getAttributeCount(); i++) {
            String attributeName = reader.getAttributeLocalName(i);

            switch(attributeName) {
                case "id": {
                    sequenceFlow.setId(reader.getAttributeValue(i));
                    break;
                }
                case "sourceRef": {
                    sequenceFlow.setSourceRef(reader.getAttributeValue(i));
                    break;
                }
                case "targetRef": {
                    sequenceFlow.setTargetRef(reader.getAttributeValue(i));
                    break;
                }
            }
        }

        sequenceFlows.add(sequenceFlow);
    }

    private void parseDiagramBlock(BPMN bpmn, XMLStreamReader reader) throws XMLStreamException {
        BPMNDiagram diagram = parseDiagram(reader);

        while(reader.hasNext()) {
            int eventType = reader.next();
            switch (eventType) {
                case START_ELEMENT: {
                    if (reader.getLocalName().equals("BPMNPlane")) {
                        parseBPMNPlaneBlock(diagram, reader);
                    }
                    break;
                }
                case END_ELEMENT: {
                    bpmn.setDiagram(diagram);
                    return;
                }
            }
        }
    }

    private BPMNDiagram parseDiagram(XMLStreamReader reader) {
        BPMNDiagram bpmnDiagram = new BPMNDiagram();

        for (int i = 0; i < reader.getAttributeCount(); i++) {
            switch(reader.getAttributeLocalName(i)) {
                case "documentation": {
                    bpmnDiagram.setDocumentation(reader.getAttributeValue(i));
                    break;
                }
                case "id": {
                    bpmnDiagram.setId(reader.getAttributeValue(i));
                    break;
                }
                case "name": {
                    bpmnDiagram.setName(reader.getAttributeValue(i));
                }
            }
        }

        return bpmnDiagram;
    }

    private void parseBPMNPlaneBlock(BPMNDiagram diagram, XMLStreamReader reader) throws XMLStreamException {
        BPMNPlane bpmnPlane = parseBPMNPLane(reader);

        while(reader.hasNext()) {
            int eventType = reader.next();
            switch (eventType) {
                case START_ELEMENT: {
                    if (reader.getLocalName().equals("BPMNShape")) {
                        parseBPMNShapeBlock(bpmnPlane, reader);
                    } else if (reader.getLocalName().equals("BPMNEdge")) {
                        parseBPMNEdgeBlock(bpmnPlane, reader);
                    }
                    break;
                }
                case END_ELEMENT: {
                    diagram.setBpmnPlane(bpmnPlane);
                    return;
                }
            }
        }
    }

    private BPMNPlane parseBPMNPLane(XMLStreamReader reader) {
        BPMNPlane bpmnPlane = new BPMNPlane();

        for (int i = 0; i < reader.getAttributeCount(); i++) {
            if (reader.getAttributeLocalName(i).equals("bpmnElement")) {
                bpmnPlane.setBpmnElement(reader.getAttributeValue(i));
            }
        }

        return bpmnPlane;
    }

    private void parseBPMNShapeBlock(BPMNPlane bpmnPlane, XMLStreamReader reader) throws XMLStreamException {
        BPMNShape bpmnShape = parseBPMNShape(reader);

        while(reader.hasNext()) {
            int eventType = reader.next();
            switch (eventType) {
                case START_ELEMENT: {
                    if (reader.getLocalName().equals("Bounds")) {
                        bpmnShape.setBounds(parseBounds(reader));
                        reader.next();
                    } else if (reader.getLocalName().equals("BPMNLabel")){
                        parseBPMNLabelBlock(bpmnShape, reader);
                    }
                    break;
                }
                case END_ELEMENT: {
                    bpmnPlane.addBpmnShape(bpmnShape);
                    return;
                }
            }
        }
    }

    private BPMNShape parseBPMNShape(XMLStreamReader reader) {
        BPMNShape bpmnShape = new BPMNShape();

        for (int i = 0; i < reader.getAttributeCount(); i++) {
            switch(reader.getAttributeLocalName(i)) {
                case "bpmnElement": {
                    bpmnShape.setBpmnElement(reader.getAttributeValue(i));
                    break;
                }
                case "id": {
                    bpmnShape.setId(reader.getAttributeValue(i));
                    break;
                }
            }
        }

        return bpmnShape;
    }

    private Bounds parseBounds(XMLStreamReader reader) {
        Bounds bounds = new Bounds();

        for (int i = 0; i < reader.getAttributeCount(); i++) {
            switch(reader.getAttributeLocalName(i)) {
                case "height": {
                    bounds.setHeight(reader.getAttributeValue(i));
                    break;
                }
                case "width": {
                    bounds.setWidth(reader.getAttributeValue(i));
                    break;
                }
                case "x": {
                    bounds.setX(reader.getAttributeValue(i));
                    break;
                }
                case "y": {
                    bounds.setY(reader.getAttributeValue(i));
                    break;
                }
            }
        }

        return bounds;
    }

    private void parseBPMNLabelBlock(BPMNObject bpmnObject, XMLStreamReader reader) throws XMLStreamException {
        BPMNLabel bpmnLabel = new BPMNLabel();

        while(reader.hasNext()) {
            int eventType = reader.next();
            switch (eventType) {
                case START_ELEMENT: {
                    if (reader.getLocalName().equals("Bounds")) {
                        bpmnLabel.setBounds(parseBounds(reader));
                        reader.next();
                    }
                    break;
                }
                case END_ELEMENT: {
                    bpmnObject.setBpmnLabel(bpmnLabel);
                    return;
                }
            }
        }
    }

    private void parseBPMNEdgeBlock(BPMNPlane bpmnPlane, XMLStreamReader reader) throws XMLStreamException {
        BPMNEdge bpmnEdge = parseBPMNEdge(reader);

        while(reader.hasNext()) {
            int eventType = reader.next();
            switch (eventType) {
                case START_ELEMENT: {
                    if (reader.getLocalName().equals("waypoint")) {
                        bpmnEdge.addWaypoint(parseWaypoint(reader));
                        reader.next();
                    } else if (reader.getLocalName().equals("BPMNLabel")) {
                        parseBPMNLabelBlock(bpmnEdge, reader);
                    }
                    break;
                }
                case END_ELEMENT: {
                    bpmnPlane.addBPMNEdge(bpmnEdge);
                    return;
                }
            }
        }
    }

    private BPMNEdge parseBPMNEdge(XMLStreamReader reader) {
        BPMNEdge bpmnEdge = new BPMNEdge();

        for (int i = 0; i < reader.getAttributeCount(); i++) {
            switch(reader.getAttributeLocalName(i)) {
                case "bpmnElement": {
                    bpmnEdge.setBpmnElement(reader.getAttributeValue(i));
                    break;
                }
                case "id": {
                    bpmnEdge.setId(reader.getAttributeValue(i));
                    break;
                }
                case "sourceElement": {
                    bpmnEdge.setSourceElement(reader.getAttributeValue(i));
                    break;
                }
                case "targetElement": {
                    bpmnEdge.setTargetElement(reader.getAttributeValue(i));
                    break;
                }
            }
        }

        return bpmnEdge;
    }

    private Waypoint parseWaypoint(XMLStreamReader reader) {
        Waypoint waypoint = new Waypoint();

        for (int i = 0; i < reader.getAttributeCount(); i++) {
            switch(reader.getAttributeLocalName(i)) {
                case "x": {
                    waypoint.setX(reader.getAttributeValue(i));
                    break;
                }
                case "y": {
                    waypoint.setY(reader.getAttributeValue(i));
                    break;
                }
            }
        }

        return waypoint;
    }

}
