import model.Action;
import model.bpmn.*;
import model.DatabaseData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class PlanBPMNParser {

    PlanBPMNParser(BPMNInterface bpmnInterface, DatabaseData databaseData, Map<Integer, List<String>> plan) throws Exception {
        analyzeBPMN(bpmnInterface, databaseData, plan);
    }

    private void analyzeBPMN(BPMNInterface bpmnInterface, DatabaseData databaseData, Map<Integer, List<String>> plan) throws Exception {
        Map<Integer, List<BPMN>> bpmnList = new HashMap<>();

        BPMNParser dataParser = new BPMNParser();

        for (int time : plan.keySet()) {
            for (String planAction : plan.get(time)) {
                String planActionName = planAction.split(" ")[0];

                Action action = getActionFromDatabase(databaseData, planActionName);
                if (action != null && action.hasBpmn()) {

                    if (!bpmnList.containsKey(time)) {
                        bpmnList.put(time, new ArrayList<>());
                    }
                    List<BPMN> list = bpmnList.get(time);
                    list.add(dataParser.getBpmnData(action.getBpmn()));
                    bpmnList.put(time, list);
                }
            }
        }

        bpmnInterface.setParsedBPMNList(bpmnList);
    }

    private Action getActionFromDatabase(DatabaseData databaseData, String name) {
        for (Action action : databaseData.getActions()) {
            if (action.getName().equals(name)) {
                return action;
            }
        }
        return null;
    }
}
