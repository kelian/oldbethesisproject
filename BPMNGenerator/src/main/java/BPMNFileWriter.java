import model.bpmn.BPMN;
import model.bpmn.BPMNProcess;
import model.bpmn.BPMNTask;
import model.bpmn.SequenceFlow;
import model.bpmn.diagram.*;
import utils.BPMNUtils;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

class BPMNFileWriter {

    private BPMN bpmn;

    BPMNFileWriter(BPMN bpmn) throws Exception {
        this.bpmn = bpmn;

        writeFile();
    }

    private void writeFile() throws Exception {
        XMLOutputFactory xmlOutFac = XMLOutputFactory.newInstance();
        StringWriter sw = new StringWriter();
        XMLStreamWriter writer = xmlOutFac.createXMLStreamWriter(sw);

        writeStartDocument(writer);
        writeDefinitions(writer);

        writer.flush();
        sw.flush();
        StringBuffer sb = sw.getBuffer();
        BPMNUtils.writeFile("bpmn.bpmn", sb.toString());
    }

    private void writeStartDocument(XMLStreamWriter writer) throws XMLStreamException {
        writer.writeStartDocument("UTF-8", "1.0");
    }

    private void writeDefinitions(XMLStreamWriter writer) throws XMLStreamException {
        writer.writeStartElement("definitions");

        writeDefinitionsNamespace(writer);
        writeDefinitionsAttributes(writer);

        writeProcess(writer);
        writeDiagram(writer);

        writer.writeEndElement();
    }

    private void writeDefinitionsNamespace(XMLStreamWriter writer) throws XMLStreamException {
        Map<String, String> nameSpaceList = bpmn.getDefinitions().getNamespaceList();
        for (String key : nameSpaceList.keySet()) {
            writer.writeNamespace(key, nameSpaceList.get(key));
        }
    }

    private void writeDefinitionsAttributes(XMLStreamWriter writer) throws XMLStreamException {
        Map<String, String> attributeList = bpmn.getDefinitions().getAttributes();
        for (String key : attributeList.keySet()) {
            writer.writeAttribute(key, attributeList.get(key));
        }
    }

    private void writeProcess(XMLStreamWriter writer) throws XMLStreamException {
        if (bpmn.getProcess() != null) {
            writer.writeStartElement("process");

            BPMNProcess process = bpmn.getProcess();
            writeAttributes(writer, process.getAttributes());
            writeProcessEvents(writer, process);
            writeSequenceFlows(writer, process.getSequenceFlows());

            writer.writeEndElement();
        }
    }

    private void writeProcessEvents(XMLStreamWriter writer, BPMNProcess process) throws XMLStreamException {
        writeProcessTask(writer, process.getStartEvent());
        writeProcessTask(writer, process.getEndEvent());

        if (process.getBpmnTasks() != null) {
            for (BPMNTask task : process.getBpmnTasks()) {
                writeProcessTask(writer, task);
            }
        }
    }

    private void writeProcessTask(XMLStreamWriter writer, BPMNTask task) throws XMLStreamException {
        writer.writeStartElement(task.getType());

        writer.writeAttribute("id", task.getId());
        writeAttributes(writer, task.getAttributes());

        writer.writeEndElement();
    }

    private void writeAttributes(XMLStreamWriter writer, Map<String, String> attributes) throws XMLStreamException {
        if (attributes != null) {
            for (String key : attributes.keySet()) {
                writer.writeAttribute(key, attributes.get(key));
            }
        }
    }

    private void writeSequenceFlows(XMLStreamWriter writer, List<SequenceFlow> sequenceFlows) throws XMLStreamException {
        if (sequenceFlows != null) {
            for (SequenceFlow sequenceFlow : sequenceFlows) {
                writer.writeStartElement("sequenceFlow");

                writer.writeAttribute("id", sequenceFlow.getId());
                writer.writeAttribute("sourceRef", sequenceFlow.getSourceRef());
                writer.writeAttribute("targetRef", sequenceFlow.getTargetRef());

                writer.writeEndElement();
            }
        }
    }

    private void writeDiagram(XMLStreamWriter writer) throws XMLStreamException {
        BPMNDiagram diagram = bpmn.getDiagram();
        if (diagram != null) {
            writer.writeStartElement("bpmndi:BPMNDiagram");

            writer.writeAttribute("documentation", diagram.getDocumentation());
            writer.writeAttribute("id", diagram.getId());
            writer.writeAttribute("name", diagram.getName());

            writePlane(writer, diagram.getBpmnPlane());

            writer.writeEndElement();
        }
    }

    private void writePlane(XMLStreamWriter writer, BPMNPlane plane) throws XMLStreamException {
        if (plane != null) {
            writer.writeStartElement("bpmndi:BPMNPlane");

            writer.writeAttribute("bpmnElement", plane.getBpmnElement());

            writeShapes(writer, plane.getBpmnShapeList());
            writeEdges(writer, plane.getBpmnEdgeList());

            writer.writeEndElement();
        }
    }

    private void writeShapes(XMLStreamWriter writer, List<BPMNShape> shapes) throws XMLStreamException {
        if (shapes != null) {
            for (BPMNShape bpmnShape: shapes) {
                writer.writeStartElement("bpmndi:BPMNShape");

                writer.writeAttribute("bpmnElement", bpmnShape.getBpmnElement());
                writer.writeAttribute("id", bpmnShape.getId());

                writeBounds(writer, bpmnShape.getBounds());
                writeLabel(writer, bpmnShape.getBpmnLabel());

                writer.writeEndElement();
            }
        }
    }

    private void writeEdges(XMLStreamWriter writer, List<BPMNEdge> edges) throws XMLStreamException {
        if (edges != null) {
            for (BPMNEdge edge : edges) {
                writer.writeStartElement("bpmndi:BPMNEdge");

                writer.writeAttribute("bpmnElement", edge.getBpmnElement());
                writer.writeAttribute("id", edge.getId());
                writer.writeAttribute("sourceElement", edge.getSourceElement());
                writer.writeAttribute("targetElement", edge.getTargetElement());

                writeWaypoints(writer, edge.getWaypointList());
                writeLabel(writer, edge.getBpmnLabel());

                writer.writeEndElement();
            }
        }
    }

    private void writeLabel(XMLStreamWriter writer, BPMNLabel label) throws XMLStreamException {
        if (label != null) {
            writer.writeStartElement("bpmndi:BPMNLabel");

            writeBounds(writer, label.getBounds());

            writer.writeEndElement();
        }
    }

    private void writeBounds(XMLStreamWriter writer, Bounds bounds) throws XMLStreamException {
        if (bounds != null) {
            writer.writeStartElement("omgdc:Bounds");

            writer.writeAttribute("height", bounds.getHeight());
            writer.writeAttribute("width", bounds.getWidth());
            writer.writeAttribute("x", bounds.getX());
            writer.writeAttribute("y", bounds.getY());

            writer.writeEndElement();
        }
    }

    private void writeWaypoints(XMLStreamWriter writer, List<Waypoint> waypoints) throws XMLStreamException {
        if (waypoints != null) {
            for (Waypoint waypoint: waypoints) {
                writer.writeStartElement("omgdi:waypoint");

                writer.writeAttribute("x", waypoint.getX());
                writer.writeAttribute("y", waypoint.getY());

                writer.writeEndElement();
            }
        }
    }

}
