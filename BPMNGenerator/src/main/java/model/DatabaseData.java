package model;

import java.util.List;

public class DatabaseData {
    private List<Action> actions;
    private List<Location> locations;

    public List<Action> getActions() {
        return actions;
    }

    public List<Location> getLocations() {
        return locations;
    }
}
