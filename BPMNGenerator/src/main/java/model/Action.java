package model;

import java.util.ArrayList;
import java.util.List;

public class Action {

    private String name;
    private List<String> parameters = new ArrayList<>();
    private String bpmn = null;

    public String getName() {
        return name;
    }

    public String getBpmn() {
        return bpmn;
    }

    public boolean hasBpmn() {
        return bpmn != null && !bpmn.isEmpty();
    }

    public List<String> getParameters() {
        return parameters;
    }
}
