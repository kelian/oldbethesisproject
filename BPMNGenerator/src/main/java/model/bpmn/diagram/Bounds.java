package model.bpmn.diagram;

public class Bounds {

    private String height;
    private String width;

    private String x;
    private String y;

    public Bounds() {

    }

    public Bounds(String height, String width, String y, String x) {
        this.height = height;
        this.width = width;
        this.y = y;
        this.x = x;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public void setHeight(float height) {
        this.height = String.valueOf(height);
    }

    public String getHeight() {
        return height;
    }

    public float getHeightFloat() {
        return Float.parseFloat(height);
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public void setWidth(float width) {
        this.width = String.valueOf(width);
    }

    public String getWidth() {
        return width;
    }

    public float getWidthFloat() {
        return Float.parseFloat(width);
    }

    public void setX(String x) {
        this.x = x;
    }

    public void setX(float x) {
        this.x = String.valueOf(x);
    }

    public String getX() {
        return x;
    }

    public float getXFloat() {
        return Float.parseFloat(x);
    }

    public void setY(String y) {
        this.y = y;
    }

    public void setY(float y) {
        this.y = String.valueOf(y);
    }

    public String getY() {
        return y;
    }

    public float getYFloat() {
        return Float.parseFloat(y);
    }


}
