package model.bpmn.diagram;

import java.util.ArrayList;
import java.util.List;

public class BPMNPlane {
    private String bpmnElement;

    private List<BPMNShape> bpmnShapeList = new ArrayList<>();
    private List<BPMNEdge> bpmnEdgeList = new ArrayList<>();

    public BPMNPlane() {

    }

    public BPMNPlane(String bpmnElement) {
        this.bpmnElement = bpmnElement;
    }

    public void setBpmnElement(String bpmnElement) {
        this.bpmnElement = bpmnElement;
    }

    public String getBpmnElement() {
        return bpmnElement;
    }

    public void addBpmnShape(BPMNShape bpmnShape) {
        bpmnShapeList.add(bpmnShape);
    }

    public List<BPMNShape> getBpmnShapeList() {
        return bpmnShapeList;
    }

    public List<BPMNEdge> getBpmnEdgeList() {
        return bpmnEdgeList;
    }

    public void addBPMNEdge(BPMNEdge bpmnEdge) {
        bpmnEdgeList.add(bpmnEdge);
    }
}
