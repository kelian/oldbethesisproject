package model.bpmn.diagram;

public class BPMNShape extends BPMNObject {

    private String bpmnElement;
    private String id;

    private Bounds bounds;

    public void setBpmnElement(String bpmnElement) {
        this.bpmnElement = bpmnElement;
    }

    public String getBpmnElement() {
        return bpmnElement;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setBounds(Bounds bounds) {
        this.bounds = bounds;
    }

    public Bounds getBounds() {
        return bounds;
    }
}
