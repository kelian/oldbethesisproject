package model.bpmn.diagram;

public class BPMNObject {

    private BPMNLabel bpmnLabel;

    public void setBpmnLabel(BPMNLabel bpmnLabel) {
        this.bpmnLabel = bpmnLabel;
    }

    public BPMNLabel getBpmnLabel() {
        return bpmnLabel;
    }
}
