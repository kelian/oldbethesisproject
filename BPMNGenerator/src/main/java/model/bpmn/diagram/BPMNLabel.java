package model.bpmn.diagram;

public class BPMNLabel {

    private Bounds bounds;

    public void setBounds(Bounds bounds) {
        this.bounds = bounds;
    }

    public Bounds getBounds() {
        return bounds;
    }
}
