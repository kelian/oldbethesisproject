package model.bpmn.diagram;

public class Waypoint {

    private String x;
    private String y;

    public void setY(String y) {
        this.y = y;
    }

    public String getY() {
        return y;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getX() {
        return x;
    }
}
