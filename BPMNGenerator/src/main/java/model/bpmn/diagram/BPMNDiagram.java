package model.bpmn.diagram;

public class BPMNDiagram {

    private String documentation;
    private String id;
    private String name;

    private BPMNPlane bpmnPlane;

    public BPMNDiagram() {

    }

    public BPMNDiagram(String documentation, String id, String name) {
        this.documentation = documentation;
        this.id = id;
        this.name = name;
    }

    public void setDocumentation(String documentation) {
        this.documentation = documentation;
    }

    public String getDocumentation() {
        return documentation;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setBpmnPlane(BPMNPlane bpmnPlane) {
        this.bpmnPlane = bpmnPlane;
    }

    public BPMNPlane getBpmnPlane() {
        return bpmnPlane;
    }
}
