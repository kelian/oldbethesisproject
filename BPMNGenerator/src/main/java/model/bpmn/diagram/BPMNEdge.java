package model.bpmn.diagram;

import java.util.ArrayList;
import java.util.List;

public class BPMNEdge extends BPMNObject {

    private String bpmnElement;
    private String id;
    private String sourceElement;
    private String targetElement;

    private List<Waypoint> waypointList = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBpmnElement() {
        return bpmnElement;
    }

    public void setBpmnElement(String bpmnElement) {
        this.bpmnElement = bpmnElement;
    }

    public String getSourceElement() {
        return sourceElement;
    }

    public void setSourceElement(String sourceElement) {
        this.sourceElement = sourceElement;
    }

    public String getTargetElement() {
        return targetElement;
    }

    public void setTargetElement(String targetElement) {
        this.targetElement = targetElement;
    }

    public void addWaypoint(Waypoint waypoint) {
        waypointList.add(waypoint);
    }

    public List<Waypoint> getWaypointList() {
        return waypointList;
    }
}
