package model.bpmn;

public class SequenceFlow {
    private String id;
    private String sourceRef;
    private String targetRef;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setSourceRef(String sourceRef) {
        this.sourceRef = sourceRef;
    }

    public String getSourceRef() {
        return sourceRef;
    }

    public void setTargetRef(String targetRef) {
        this.targetRef = targetRef;
    }

    public String getTargetRef() {
        return targetRef;
    }
}
