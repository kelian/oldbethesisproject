import model.bpmn.BPMN;

import java.util.List;
import java.util.Map;

public interface BPMNInterface {

    void setParsedBPMNList(Map<Integer, List<BPMN>> bpmnList) throws Exception;
    void setWholeBPMN(BPMN wholePlan) throws Exception;
}
