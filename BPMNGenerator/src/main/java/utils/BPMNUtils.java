package utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class BPMNUtils {

    public static String getEmptyBPMNFile() {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<definitions xmlns=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:activiti=\"http://activiti.org/bpmn\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:omgdc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:omgdi=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:tns=\"http://www.activiti.org/test\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" expressionLanguage=\"http://www.w3.org/1999/XPath\" id=\"m1557212284536\" name=\"\" targetNamespace=\"http://www.activiti.org/test\" typeLanguage=\"http://www.w3.org/2001/XMLSchema\">\n" +
                "  <process id=\"myProcess_1\" isClosed=\"false\" isExecutable=\"true\" processType=\"None\">\n" +
                "    <startEvent id=\"_2\" name=\"StartEvent\"/>\n" +
                "    <endEvent id=\"_3\" name=\"EndEvent\"/>\n" +
                "    <sequenceFlow id=\"_4\" sourceRef=\"_2\" targetRef=\"_3\"/>\n" +
                "  </process>\n" +
                "  <bpmndi:BPMNDiagram documentation=\"background=#3C3F41;count=1;horizontalcount=1;orientation=0;width=842.4;height=1195.2;imageableWidth=832.4;imageableHeight=1185.2;imageableX=5.0;imageableY=5.0\" id=\"Diagram-_1\" name=\"New Diagram\">\n" +
                "    <bpmndi:BPMNPlane bpmnElement=\"myProcess_1\">\n" +
                "      <bpmndi:BPMNShape bpmnElement=\"_2\" id=\"Shape-_2\">\n" +
                "        <omgdc:Bounds height=\"32.0\" width=\"32.0\" x=\"95.0\" y=\"255.0\"/>\n" +
                "        <bpmndi:BPMNLabel>\n" +
                "          <omgdc:Bounds height=\"32.0\" width=\"32.0\" x=\"0.0\" y=\"0.0\"/>\n" +
                "        </bpmndi:BPMNLabel>\n" +
                "      </bpmndi:BPMNShape>\n" +
                "      <bpmndi:BPMNShape bpmnElement=\"_3\" id=\"Shape-_3\">\n" +
                "        <omgdc:Bounds height=\"32.0\" width=\"32.0\" x=\"375.0\" y=\"255.0\"/>\n" +
                "        <bpmndi:BPMNLabel>\n" +
                "          <omgdc:Bounds height=\"32.0\" width=\"32.0\" x=\"0.0\" y=\"0.0\"/>\n" +
                "        </bpmndi:BPMNLabel>\n" +
                "      </bpmndi:BPMNShape>\n" +
                "      <bpmndi:BPMNEdge bpmnElement=\"_4\" id=\"BPMNEdge__4\" sourceElement=\"_2\" targetElement=\"_3\">\n" +
                "        <omgdi:waypoint x=\"127.0\" y=\"271.0\"/>\n" +
                "        <omgdi:waypoint x=\"375.0\" y=\"271.0\"/>\n" +
                "        <bpmndi:BPMNLabel>\n" +
                "          <omgdc:Bounds height=\"0.0\" width=\"0.0\" x=\"0.0\" y=\"0.0\"/>\n" +
                "        </bpmndi:BPMNLabel>\n" +
                "      </bpmndi:BPMNEdge>\n" +
                "    </bpmndi:BPMNPlane>\n" +
                "  </bpmndi:BPMNDiagram>\n" +
                "</definitions>";
    }

    public static void writeFile(String fileName, String file) throws Exception {
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
        if (file != null) {
            writer.write(file);
        } else {
            throw new Exception("Error while writing to file: " + fileName);
        }
        writer.close();
    }

    public static void writeBPMNFile(String fileName, String file) throws Exception {
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName + ".bpmn"));
        if (file != null) {
            writer.write(file);
        } else {
            throw new Exception("Error while writing to file: " + fileName);
        }
        writer.close();
    }

    public static void deleteFile(String fileName) {
        File file = new File(fileName);
        file.delete();
    }

}
