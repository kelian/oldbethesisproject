import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import model.DatabaseData;
import model.bpmn.BPMN;
import utils.BPMNUtils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BPMNGenerator implements BPMNInterface {

    private Map<Integer, List<String>> plan = new HashMap<>();
    private Map<Integer, List<BPMN>> planBPMNList;

    private DatabaseData databaseData;

    public BPMNGenerator(String plannerOutput) throws Exception {
        getPlan(plannerOutput);
        getDatabaseData();

        new PlanBPMNParser(this, databaseData, plan);
    }

    private void getDatabaseData() throws FileNotFoundException {
        Gson gson = new Gson();
        JsonReader jsonReader = new JsonReader(new FileReader("database_response.json"));
        databaseData = gson.fromJson(jsonReader, DatabaseData.class);
    }

    private void getPlan(String plannerOutput) {
        String[] output = plannerOutput.split("\n");
        for(String row : output) {
            if (isPlanRow(row)) {
                String[] rowSplit = row.split(":");
                int time = Integer.parseInt(rowSplit[0]);
                String predicate = rowSplit[1].substring(1, rowSplit[1].length() - 1);

                if (!plan.containsKey(time))
                    plan.put(time, new ArrayList<>());

                List<String> planList = plan.get(time);
                planList.add(predicate);
                plan.put(time, planList);
            }
        }
    }

    private boolean isPlanRow(String row) {
        return !row.startsWith("Time") && !row.startsWith(";");
    }

    private void parseBaseBPMN() throws Exception {
        BPMN baseBPMN = new BPMNParser().getBpmnData(BPMNUtils.getEmptyBPMNFile());
        new WholeBPMNGenerator(this, baseBPMN, planBPMNList);
    }

    @Override
    public void setParsedBPMNList(Map<Integer, List<BPMN>> bpmnList) throws Exception {
        this.planBPMNList = bpmnList;
        parseBaseBPMN();
    }

    @Override
    public void setWholeBPMN(BPMN wholePlan) throws Exception {
        new BPMNFileWriter(wholePlan);
    }
}
