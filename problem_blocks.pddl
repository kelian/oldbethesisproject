(define (problem blocks_problem)
    (:domain blocks_domain)

    (:objects
        b1 b2 b3 b4 - block
    )

    (:init
        (clear b1)
        (ontable b1)
        (clear b2)
        (ontable b2)
        (clear b3)
        (ontable b3)
        (clear b4)
        (ontable b4)
        (handempty)
    )

    (:goal
        (and (on b2 b1) (on b3 b2) (on b4 b3))
    )

)