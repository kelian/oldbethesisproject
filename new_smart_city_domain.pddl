(define (domain new_smart_city_domain)
    (:requirements :strips :typing :negative-preconditions :disjunctive-preconditions :conditional-effects)

    (:types action_feed_buckbeak action_pet_fang action_swim_with_mermaids action_play_HP_Soundtrack - action
            location - object
    )

    (:predicates
        (has_weather_data)
        (at ?location - location)
        (action_at ?action - action ?location - location)
        (action_in_progress)
        (action_is_executed ?action - action)
    )

    (:action get_weather_data
        :parameters ()
        :precondition (not (has_weather_data))
        :effect (has_weather_data)
    )

    (:action feed_buckbeak
        :parameters (?location_first - location ?action - action_feed_buckbeak)
        :precondition (and (at ?location_first) (action_at ?action ?location_first) (action_in_progress) (not (action_is_executed ?action)))
        :effect (and (not (action_in_progress)) (action_is_executed ?action))
    )

    (:action pet_fang
        :parameters (?location_first - location ?action - action_pet_fang)
        :precondition (and (at ?location_first) (action_at ?action ?location_first) (action_in_progress) (not (action_is_executed ?action)))
        :effect (and (not (action_in_progress)) (action_is_executed ?action))
    )

    (:action swim_with_mermaids
        :parameters (?location_first - location ?action - action_swim_with_mermaids)
        :precondition (and (has_weather_data) (at ?location_first) (action_at ?action ?location_first) (action_in_progress) (not (action_is_executed ?action)))
        :effect (and (not (action_in_progress)) (action_is_executed ?action))
    )

    (:action play_HP_Soundtrack
        :parameters (?location_first - location ?action - action_play_HP_Soundtrack)
        :precondition (and (at ?location_first) (action_at ?action ?location_first) (action_in_progress) (not (action_is_executed ?action)))
        :effect (and (not (action_in_progress)) (action_is_executed ?action))
    )

    (:action move_to
        :parameters (?location_1 - location ?location_2 - location)
        :precondition (at ?location_1)
        :effect (and (not (at ?location_1)) (at ?location_2))
    )

    (:action start_action
        :parameters ()
        :precondition (not (action_in_progress))
        :effect (action_in_progress)
    )
)