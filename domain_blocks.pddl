(define (domain blocks_domain)
    (:requirements :strips :typing :negative-preconditions :disjunctive-preconditions :conditional-effects)

    (:types block - object
    )

    (:predicates
        (clear ?block - block)
        (ontable ?block - block)
        (handempty)
        (holding ?block - block)
        (on ?block1 - block ?block2 - block)
    )

    (:action pick-up
        :parameters (?block1 - block)
        :precondition (and (clear ?block1) (ontable ?block1) (handempty))
        :effect (and (not (ontable ?block1)) (not (clear ?block1)) (not (handempty)) (holding ?block1))
    )

    (:action put-down
        :parameters (?block1 - block)
        :precondition (holding ?block1)
        :effect (and (not (holding ?block1)) (clear ?block1) (handempty) (ontable ?block1))
    )

    (:action stack
        :parameters (?block1 - block ?block2 - block)
        :precondition (and (holding ?block1) (clear ?block2))
        :effect (and (not (holding ?block1)) (not (clear ?block2)) (clear ?block1) (handempty) (on ?block1 ?block2))
    )

    (:action unstack
        :parameters (?block1 - block ?block2 - block)
        :precondition (and (on ?block1 ?block2) (clear ?block1) (handempty))
        :effect (and (holding ?block1) (clear ?block2) (not (clear ?block1)) (not (handempty)) (not (on ?block1 ?block2)))
    )

)