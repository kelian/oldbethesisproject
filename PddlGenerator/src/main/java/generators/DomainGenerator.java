package generators;

import model.ActionData;
import model.database.Action;
import model.database.Helper;

import java.util.*;

import static utils.Constants.*;

public class DomainGenerator {

    private static final String PARAMETER_ACTION = "action";
    private static final String PARAMETER_CURRENT_LOCATION = "currentLocation";
    private static final String PARAMETER_LOCATION_ID = "locationId";
    private static final String PARAMETER_LOCATION = "location";
    private static final String PARAMETER_LIST = "list";

    private PDDLInterface pddlInterface;

    private List<ActionData> actionDataList = new ArrayList<>();

    private Map<Integer, Integer> locationActionCountList = new HashMap<>();

    private Boolean shouldAddLocationCompulsoryActions = false;

    private int maxNrOfTasksForLocation = 0;

    public DomainGenerator(PDDLInterface pddlInterface, List<Action> actions, List<Action> actionTypes, List<Helper> helpers) throws Exception {
        this.pddlInterface = pddlInterface;

        generateActionDataList(actions, actionTypes, helpers);
    }

    private void generateActionDataList(List<Action> actions, List<Action> actionTypes, List<Helper> helpers) throws Exception {
        setUpActionList(actions, actionTypes, helpers);

        for (Action action : actions) {
            createAction(action);
        }

       //  getMaxNumberOfTasksForLocation();
//
//        if (shouldAddLocationCompulsoryActions)
//            createLocationCompulsoryActions();
//
//        if (pddlInterface.getNrOfLocationsToDo() > 0)
//            createLocationToDoAction();
    }

    private void setUpActionList(List<Action> actions, List<Action> actionTypes, List<Helper> helpers) {
        List<Action> helperActions = new ArrayList<>();
        for (Action action : actions) {
            setActionDataFromTypes(action, actionTypes);
            setActionDataFromHelpers(action, helpers, helperActions);
        }

        actions.addAll(helperActions);
    }

    private void setActionDataFromTypes(Action action, List<Action> actionTypes) {
        if (action.hasType()) {
            for (Action type : actionTypes) {
                if (action.getType().equals(type.getName())) {
                    action.addPreconditions(type.getPreconditions());
                    action.addEffects(type.getEffects());
                }
            }
        }
    }

    private void setActionDataFromHelpers(Action action, List<Helper> helpers, List<Action> helperActions) {
        if (action.hasHelpers()) {
            for (String helperName : action.getHelpers().keySet()) {
                setActionDataFromHelper(action, helperName, helpers, helperActions);
            }
        }
    }

    private void setActionDataFromHelper(Action action, String helperName, List<Helper> helpers, List<Action> helperActions) {
        for (Helper helper: helpers) {
            if (helper.getName().equals(helperName)) {
                action.addPreconditions(helper.getPreconditions());
                action.addEffects(helper.getEffects());

                addHelperInit(helperName, action);
                addHelperActions(helper, helperActions);
                break;
            }
        }
    }

    private void addHelperInit(String helperName, Action action) {
        List<String> helperInit = action.getHelpers().get(helperName);
        analyzeInit(helperInit, action.getName());
    }

    private void addHelperActions(Helper helper, List<Action> helperActions) {
        for (Action action: helper.getActions()) {
            if(!helperActions.contains(action)) {
                helperActions.add(action);
            }
        }
    }

    private void createAction(Action action) throws Exception {
        ActionData actionData = new ActionData(action.getName());

        analyzePreconditions(actionData, action.getPreconditions());
        analyzeEffects(actionData, action.getEffects());
        analyzeInit(action.getInit(), action.getName());

//        if (action.isLocationCompulsoryAction())
//            addLocationCompulsoryParameters(actionData);
//
//        addActionToLocationActionsList(action);

        formatActionItems(actionData);

        actionDataList.add(actionData);
    }

    private void formatActionItems(ActionData actionData) {
        List<String> formattedPreconditions = new ArrayList<>();
        for (String precondition: actionData.getPreconditions()) {
            formattedPreconditions.add(precondition.replace("-", "_"));
        }
        actionData.setPreconditions(formattedPreconditions);

        List<String> formattedEffects = new ArrayList<>();
        for (String effect : actionData.getEffects()) {
            formattedEffects.add(effect.replace("-", "_"));
        }
        actionData.setEffects(formattedEffects);

    }

    private void analyzePreconditions(ActionData actionData, List<String> preconditionList) throws Exception {
        for (String precondition : preconditionList) {
            analyzeActionItem(actionData, precondition);
            actionData.addPrecondition(precondition);
        }
    }

    private void analyzeEffects(ActionData actionData, List<String> effectList) throws Exception {
        for (String effect : effectList) {
            analyzeActionItem(actionData, effect);
            actionData.addEffect(effect);
        }
    }

    private void analyzeInit(List<String> initList, String actionName) {
        for (String init: initList) {
            String[] itemList = init.split(" ");

            int parameterStartId = 1;
            String fullInit = itemList[0];
            if (itemList[0].equals(PARAMETER_NOT)) {
                fullInit += " " + itemList[1];
                parameterStartId = 2;
            }

            for (String parameter : Arrays.asList(itemList).subList(parameterStartId, itemList.length)) {
                fullInit += getInitParameter(parameter, actionName);
            }

            pddlInterface.addInit(fullInit);
        }
    }

    private String getInitParameter(String parameter, String actionName) {
        if (parameter.equals(PARAMETER_CURRENT_LOCATION)) {
            return addCurrentLocation();
        } else if (parameter.equals(PARAMETER_ACTION)) {
            return addAction(actionName);
        } else if (parameter.startsWith(PARAMETER_LOCATION_ID)) {
            return addLocationId(parameter);
        } else if (parameter.startsWith(PARAMETER_LIST)) {
            return addItemList(parameter);
        }
        return "";
    }

    private String addCurrentLocation() {
        String currentLocation = pddlInterface.getCurrentLocation();
        if (currentLocation != null && !currentLocation.isEmpty()) {

            pddlInterface.addObject(PARAMETER_LOCATION, currentLocation);

            return " " + currentLocation;
        }
        return "";
    }

    private String addAction(String actionName) {
        String actionObjectName = PARAMETER_ACTION + "_" + actionName;
        pddlInterface.addObject(actionObjectName, actionName);

        return " " + actionName;
    }

    private String addLocationId(String parameter) {
        String[] initArray = parameter.split("=");
        int locationId = Integer.parseInt(initArray[initArray.length - 1]);
        String locationName = pddlInterface.getLocationName(locationId);

        if (locationName != null && !locationName.isEmpty()) {

            pddlInterface.addObject(PARAMETER_LOCATION, locationName);

            return " " + locationName;
        }

        return "";
    }

    private String addItemList(String parameter) {
        String[] array = parameter.split("=");
        System.out.println(array[1]);

        return "";
    }

    private void analyzeActionItem(ActionData actionData, String item) throws Exception {
        String[] itemArray = item.split(" ");

        String predicate = getPredicate(itemArray);
        List<String> parameterList = getParameters(itemArray);

        getDomainType(actionData.getName(), parameterList);
        getDomainPredicate(predicate, parameterList);
        getActionParameters(actionData, parameterList);
    }

    private void getDomainType(String actionName, List<String> parameterList) throws Exception {
        for (String parameter : parameterList) {
            String object = getObject(parameter);
            if (object.equals(PARAMETER_ACTION)) {
                addTypeObjectAction(actionName);
            } else {
                pddlInterface.addTypeObject(object);
            }
        }
    }

    private void addTypeObjectAction(String actionName) {
        String typeObject = PARAMETER_ACTION + "_" + actionName;
        pddlInterface.addType(PARAMETER_ACTION, typeObject);
    }

    private void getDomainPredicate(String predicateName, List<String> parameterList) throws Exception {
        StringBuilder predicate = new StringBuilder(predicateName);

        Map<String, Integer> objectMap = new HashMap<>();
        for (String parameter : parameterList) {
            String object = getObject(parameter);
            if (objectMap.containsKey(object)) {
                int objectCount = objectMap.get(object);
                objectCount += 1;
                objectMap.put(object, objectCount);
            } else {
                objectMap.put(object, 1);
            }
        }

        for (String object : objectMap.keySet()) {
            int objectCount = objectMap.get(object);
            if (objectCount > 1) {
                for (int i = 1; i <= objectCount; i++) {
                    predicate.append(" ?").append(object).append(i).append(" - ").append(object);
                }
            } else {
                predicate.append(" ?").append(object).append(" - ").append(object);
            }
        }

        pddlInterface.addPredicate(predicate.toString());
    }

    private void getActionParameters(ActionData actionData, List<String> parameterList) throws Exception {
        for (String parameter : parameterList) {
            String actionParameter = getFormattedParameter(parameter) + " - " + getParameterObject(parameter, actionData.getName());
            actionData.addParameter(actionParameter);
        }
    }

    private String getParameterObject(String parameter, String actionName) throws Exception {
        String object = getObject(parameter);
        if (object.equals(PARAMETER_ACTION)) {
            return PARAMETER_ACTION + "_" + actionName;
        } else {
            return object;
        }
    }

    private String getFormattedParameter(String parameter) {
        return parameter.replace("-", "_");
    }

    private String getPredicate(String[] itemArray) {
        if (itemArray[0].equals(PARAMETER_NOT)) {
            return itemArray[1];
        } else {
            return itemArray[0];
        }
    }

    private List<String> getParameters(String[] itemArray) {
        if (itemArray[0].equals(PARAMETER_NOT)) {
            if (itemArray.length > 2)
                return Arrays.asList(itemArray).subList(2, itemArray.length);
        } else {
            if (itemArray.length > 1)
                return Arrays.asList(itemArray).subList(1, itemArray.length);
        }
        return new ArrayList<>();
    }

    private String getObject(String parameter) throws Exception {
        String[] parameterArray = parameter.split("-");
        if (parameterArray.length != 0) {
            return parameterArray[0].substring(1);
        } else {
            throw new Exception("Error while parsing preconditions and effects");
        }
    }

    public List<ActionData> getActionDataList() {
        return actionDataList;
    }

    public Map<Integer, Integer> getLocationActionCountList() {
        return locationActionCountList;
    }

//    private void addActionToLocationActionsList(Action action) {
//        int locationId = action.getLocationId();
//        if (locationId != Action.NO_LOCATION_SET) {
//            int nrOfTasks = 0;
//            if (locationActionCountList.containsKey(locationId)) {
//                nrOfTasks = locationActionCountList.get(locationId);
//            }
//            nrOfTasks += 1;
//            locationActionCountList.put(locationId, nrOfTasks);
//
//            pddlInterface.addLocationAction(locationId, action);
//        }
//    }
//
//    private void addLocationCompulsoryParameters(ActionData actionData) {
//        shouldAddLocationCompulsoryActions = true;
//
//        actionData.addPrecondition(PARAMETER_NOT + " " + PARAMETER_ACTION_DONE + " ?task1");
//        actionData.addEffect(PARAMETER_ACTION_DONE + " ?task1");
//    }
//
//    private void createLocationToDoAction() {
//        pddlInterface.addTypeObject("goal");
//
//        addGoalPredicates();
//
//        String name = "set_location_goal_done";
//        List<String> parameters = Arrays.asList("?goal1 - goal", "?location1 - location");
//        List<String> preconditions = Arrays.asList(
//                PARAMETER_NOT + " " + PARAMETER_GOAL_DONE + " ?goal1",
//                PARAMETER_LOCATION_DONE + " ?location1",
//                PARAMETER_NOT + " " + PARAMETER_GOAL_LOCATION_DONE + " ?location1");
//        List<String> effects = Arrays.asList(
//                PARAMETER_GOAL_DONE + " ?goal1",
//                PARAMETER_GOAL_LOCATION_DONE + " ?location1");
//
//        actionDataList.add(new ActionData(name, parameters, preconditions, effects));
//    }
//
//    private void addGoalPredicates() {
//        pddlInterface.addPredicate(PARAMETER_GOAL_DONE + " ?goal - goal");
//        pddlInterface.addPredicate(PARAMETER_GOAL_LOCATION_DONE + " ?location - location");
//    }
//
//    private void getMaxNumberOfTasksForLocation() {
//        int maxNrOfTasks = 0;
//        for (int nrOfTasks : locationActionCountList.values()) {
//            if (nrOfTasks > maxNrOfTasks) {
//                maxNrOfTasks = nrOfTasks;
//            }
//        }
//
//        maxNrOfTasksForLocation = maxNrOfTasks;
//    }
//
//    private void createLocationCompulsoryActions() {
//        addLocationCompulsoryPredicates();
//
//        if (maxNrOfTasksForLocation > 0) {
//            createSetLocationDoneTasks(maxNrOfTasksForLocation);
//        }
//    }
//
//    private void addLocationCompulsoryPredicates() {
//        pddlInterface.addPredicate(PARAMETER_ACTION_DONE + " ?task - task");
//        pddlInterface.addPredicate(PARAMETER_LOCATION_DONE + " ?location - location");
//    }
//
//    private void createSetLocationDoneTasks(int maxNrOfTasks) {
//        for (int i = 1; i <= maxNrOfTasks; i++) {
//            String name = "set_location_done_" + i;
//
//            List<String> parameters = getLocationDoneParameters(i);
//            List<String> preconditions = getLocationDonePreconditions(i);
//            List<String> effects = Collections.singletonList(PARAMETER_LOCATION_DONE + " ?location1");
//
//            actionDataList.add(new ActionData(name, parameters, preconditions, effects));
//
//            addPredicate_HasTasks(i);
//        }
//    }

//    private List<String> getLocationDoneParameters(int nrOfTasks) {
//        List<String> parameters = new ArrayList<>();
//
//        parameters.add("?location1 - location");
//
//        for (int i = 1; i <= nrOfTasks; i++) {
//            String parameter = "?task" + i + " - task";
//            parameters.add(parameter);
//        }
//
//        return parameters;
//    }
//
//    private List<String> getLocationDonePreconditions(int nrOfTasks) {
//        List<String> preconditions = new ArrayList<>();
//
//        preconditions.add(PARAMETER_NOT + " " + PARAMETER_LOCATION_DONE + " ?location1");
//        preconditions.add(getLocationDonePrecondition_HasTasks(nrOfTasks));
//
//        for (int i = 1; i <= nrOfTasks; i++) {
//            String parameter = PARAMETER_ACTION_DONE + " ?task" + i;
//            preconditions.add(parameter);
//        }
//
//        return preconditions;
//    }

//    private String getLocationDonePrecondition_HasTasks(int nrOfTasks) {
//        StringBuilder precondition = new StringBuilder(PARAMETER_HAS_ACTIONS + "_" + nrOfTasks + " ?location1");
//
//        for (int i = 1; i <= nrOfTasks; i++) {
//            precondition.append(" ?task").append(i);
//        }
//
//        return precondition.toString();
//    }
//
//    private void addPredicate_HasTasks(int nrOfTasks) {
//        StringBuilder predicate = new StringBuilder(PARAMETER_HAS_ACTIONS + "_" + nrOfTasks + " ?location - location");
//
//        for (int i = 1; i <= nrOfTasks; i++) {
//            predicate.append(" ?task").append(i).append(" - task");
//        }
//
//        pddlInterface.addPredicate(predicate.toString());
//    }

//    private boolean hasActionInList(String name) {
//        for (ActionData actionData : actionDataList) {
//            if (actionData.getName().equals(name)) {
//                return true;
//            }
//        }
//        return false;
//    }

}
