package generators;

import model.PhoneData;
import model.database.Action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ActionFilter {

    private PDDLInterface pddlInterface;

    public ActionFilter(PDDLInterface pddlInterface, PhoneData phoneData, List<Action> actions) throws Exception {
        this.pddlInterface = pddlInterface;

        filterActionList(phoneData, actions);
    }

    private void filterActionList(PhoneData phoneData, List<Action> actions) throws Exception {
        List<Action> filteredActions = new ArrayList<>();
        Map<String, Boolean> phoneSpecifications = phoneData.getPhoneSpecifications();

        for (Action action : actions) {
            boolean addAction = true;
            for (String specification : phoneSpecifications.keySet()) {

                if (phoneDoesNotHaveThisSpecification(specification, phoneSpecifications, action.getSpecifications())) {
                    addAction = false;
                }
            }

            if (addAction) {
                filteredActions.add(action);
            }
        }

        pddlInterface.setFilteredActions(filteredActions);
    }

    private Boolean phoneDoesNotHaveThisSpecification(String specification, Map<String, Boolean> phoneSpecifications,
                                                      Map<String, Boolean> actionSpecifications) {
        return !phoneSpecifications.get(specification) &&
                actionSpecifications != null &&
                actionSpecifications.containsKey(specification) &&
                actionSpecifications.get(specification);
    }
}
