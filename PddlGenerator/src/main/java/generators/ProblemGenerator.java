package generators;

import model.database.Action;
import model.database.DatabaseData;
import model.database.Location;
import model.database.Problem;

import java.util.List;
import java.util.Map;

import static utils.Constants.*;

public class ProblemGenerator {

    private PDDLInterface pddlInterface;

    public ProblemGenerator(PDDLInterface pddlInterface, DatabaseData databaseData) {
        this.pddlInterface = pddlInterface;

        init(databaseData);
    }

    private void init(DatabaseData databaseData) {
        Problem problem = databaseData.getProblem();

        createObjects(databaseData.getLocations(), problem);
        createInit(problem, databaseData.getFilteredActions());
        createGoals(problem);
    }

    private void createObjects(List<Location> locations, Problem problem) {
        //createObjectsFromLocations(locations);
        createObjectsFromProblem(problem);
       // createObjectsFromGoal();
    }

    private void createObjectsFromProblem(Problem problem) {
        for (String key: problem.getObjects().keySet()) {
            for (String value: problem.getObjects().get(key)) {
                pddlInterface.addObject(key, value);
            }
        }
    }

    private void createInit(Problem problem, List<Action> actions) {
        //createInitFromCurrentLocation();
        createInitFromProblem(problem);
        //createInitFromGoals();

//        createInitFromLocationAction();
//        createInitFromSequentialActions(actions);
    }

    private void createInitFromProblem(Problem problem) {
        for (String init: problem.getInit()) {
            pddlInterface.addInit(init);
        }
    }

    private void createGoals(Problem problem) {
        //createGoalsFromGoals();
        createGoalsFromProblem(problem);
    }

    private void createGoalsFromProblem(Problem problem) {
        for (String goal: problem.getGoal()) {
            pddlInterface.addGoal(goal);
        }
    }

//    private void createObjectsFromLocations(List<Location> locations) {
//        if (shouldAddObjectCurrentLocation())
//            pddlInterface.addObject(TYPE_LOCATION, pddlInterface.getCurrentLocation());
//
//        for (int key : pddlInterface.getLocationActionCountList().keySet()) {
//            for (Location location : locations) {
//                if (location.getId() == key)
//                    pddlInterface.addObject(TYPE_LOCATION, location.getName());
//            }
//        }
//    }


//    private void createObjectsFromGoal() {
//        int goals = pddlInterface.getNrOfLocationsToDo();
//
//        if (goals > 0) {
//            for (int i = 1; i <= goals; i++) {
//                String value =  "goal_" + i + " ";
//                pddlInterface.addObject(TYPE_GOAL, value);
//            }
//        }
//    }

//
//    private void createInitFromCurrentLocation() {
//        String currentLocation = pddlInterface.getCurrentLocation();
//        if (currentLocation != null && !currentLocation.isEmpty()) {
//            pddlInterface.addInit("at " + currentLocation);
//        }
//    }


//    private void createInitFromGoals() {
//        if (pddlInterface.getNrOfLocationsToDo() > 0) {
//            for (int i = 1; i <= pddlInterface.getNrOfLocationsToDo(); i++) {
//                pddlInterface.addInit(PARAMETER_NOT + " " + PARAMETER_GOAL_DONE + " goal_" + i);
//            }
//        }
//    }
//
//    private void createInitFromLocationAction() {
//        Map<String, List<Action>> locationActions = pddlInterface.getLocationActionList();
//
//        createInitFromLocationAction_ActionAt(locationActions);
//        createInitFromLocationAction_LocationDone(locationActions);
//        createInitFromLocationAction_LocationGoalDone(locationActions);
//        createInitFromLocationAction_HasActions(locationActions);
//    }

//    private void createInitFromLocationAction_ActionAt( Map<String, List<Action>> locationActions) {
//        for (String location : locationActions.keySet()) {
//            for (Action action: locationActions.get(location)) {
//                pddlInterface.addInit("action_at " + action.getName() + " " + location);
//            }
//        }
//    }
//
//    private void createInitFromLocationAction_LocationDone(Map<String, List<Action>> locationActions) {
//        for (String location : locationActions.keySet()) {
//            for (Action action: locationActions.get(location)) {
//                if (action.isGoal()) {
//                    pddlInterface.addInit(PARAMETER_NOT + " " + PARAMETER_LOCATION_DONE + " " + location);
//                }
//            }
//        }
//    }
//
//    private void createInitFromLocationAction_LocationGoalDone(Map<String, List<Action>> locationActions) {
//        for (String location : locationActions.keySet()) {
//            for (Action action: locationActions.get(location)) {
//                if (action.isGoal()) {
//                    pddlInterface.addInit(PARAMETER_NOT + " " + PARAMETER_GOAL_LOCATION_DONE + " " + location);
//                }
//            }
//        }
//    }
//
//    private void createInitFromLocationAction_HasActions( Map<String, List<Action>> locationActions) {
//        for (String location : locationActions.keySet()) {
//            List<Action> actions = locationActions.get(location);
//
//            if (actions.size() > 0) {
//                StringBuilder init = new StringBuilder(PARAMETER_HAS_ACTIONS + "_" + actions.size() + " " + location);
//
//                for (Action action: actions) {
//                    init.append(" ").append(action.getName());
//                }
//
//                pddlInterface.addInit(init.toString());
//            }
//        }
//    }
//
//    private void createInitFromSequentialActions(List<Action> actions) {
//        for (Action action: actions) {
//            if (action.isSequential()) {
//                pddlInterface.addInit(PARAMETER_NOT + " " + PARAMETER_ACTION_DONE + " " + action.getName());
//            }
//        }
//    }
//
//    private void createGoalsFromGoals() {
//        if (pddlInterface.getNrOfLocationsToDo() > 0) {
//            for (int i = 1; i <= pddlInterface.getNrOfLocationsToDo(); i++) {
//                pddlInterface.addGoal(PARAMETER_GOAL_DONE + " goal_" + i);
//            }
//        }
//    }

//    private boolean shouldAddObjectCurrentLocation() {
//        return pddlInterface.getCurrentLocation() != null && !pddlInterface.getCurrentLocation().isEmpty();
//    }
}
