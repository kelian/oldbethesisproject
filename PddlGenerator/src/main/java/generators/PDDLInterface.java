package generators;

import model.database.Action;
import model.database.Location;

import java.util.List;
import java.util.Map;

public interface PDDLInterface {

    String getProblemFileName();
    String getDomainFileName();
    String getCurrentLocation();
    String getLocationName(int locationId);

    Map<Integer, Integer> getLocationActionCountList();

    Map<String, List<String>> getTypes();
    Map<String, List<String>> getObjects();
    Map<String, List<Action>> getLocationActionList();

    List<String> getPredicates();
    List<String> getInit();
    List<String> getGoals();

    int getNrOfLocationsToDo();

    void addTypeObject(String type);
    void addType(String key, String type);
    void addPredicate(String predicate);

    void addObject(String key, String name);
    void addInit(String init);
    void addGoal(String goal);

    void addLocationAction(int locationId, Action action);

    void setFilteredActions(List<Action> filteredActions) throws Exception;

    void setDomainFile(String domainFileText) throws Exception;
    void setProblemFile(String problemFileText) throws Exception;

}
