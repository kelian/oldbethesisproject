package filewriter;

import generators.PDDLInterface;

import java.util.List;
import java.util.Map;

import static utils.Constants.PARAMETER_NOT;

public class ProblemFileWriter {

    private PDDLInterface pddlInterface;
    private StringBuilder file = new StringBuilder();

    public ProblemFileWriter(PDDLInterface pddlInterface) throws Exception {
        this.pddlInterface = pddlInterface;

        init();
    }

    private void init() throws Exception {
        addStart(pddlInterface.getProblemFileName(), pddlInterface.getDomainFileName());
        addObjects();
        addInit();
        addGoals();
        addEnd();

        pddlInterface.setProblemFile(file.toString());
    }

    private void addStart(String problemFileName, String domainFileName) {
        file.append("(define (problem ").append(problemFileName).append(")").append(getEnd());
        file.append(getTab());
        file.append("(:domain ").append(domainFileName).append(")").append(getEnd());
        file.append(getEnd());
    }

    private void addObjects() {
        file.append(getTab());
        file.append("(:objects").append(getEnd());

        addObjectsFromGenerator();

        file.append(getTab());
        file.append(")").append(getEnd());
        file.append(getEnd());
    }

    private void addObjectsFromGenerator() {
        Map<String, List<String>> objects = pddlInterface.getObjects();

        if(!objects.isEmpty()) {
            for (String key: objects.keySet()) {
                file.append(getTwoTabs());

                for (String value : objects.get(key)) {
                    file.append(value).append(" ");
                }

                file.append("- ").append(key).append(getEnd());
            }
        }
    }

    private void addInit() {
        file.append(getTab());
        file.append("(:init").append(getEnd());

        addInitFromGenerator();

        file.append(getTab());
        file.append(")").append(getEnd());
        file.append(getEnd());
    }

    private void addInitFromGenerator() {
        List<String> initList = pddlInterface.getInit();

        for (String init: initList) {
            file.append(getTwoTabs());
            file.append("(");
            addItem(init);
            file.append(")").append(getEnd());
        }
    }

    private void addGoals() {
        file.append(getTab());
        file.append("(:goal").append(getEnd());
        file.append(getTwoTabs()).append("(");

        createGoalsFromList(pddlInterface.getGoals());

        file.append(")").append(getEnd());
        file.append(getTab());
        file.append(")").append(getEnd());
        file.append(getEnd());
    }


    private void createGoalsFromList(List<String> goalList) {
        if (goalList.size() > 1) {
            addGoalWithAnd(goalList);
        } else if (goalList.size() > 0) {
            addItem(goalList.get(0));
        }
    }

    private void addGoalWithAnd(List<String> goalList) {
        file.append("and ");

        for (String goal : goalList) {
            file.append("(");
            addItem(goal);
            file.append(") ");
        }

        file.deleteCharAt(file.length() - 1);
    }

    private void addItem(String item) {
        String[] itemArray = item.split(" ");

        if (itemArray[0].equals(PARAMETER_NOT)) {
            addItemWithNot(item);
        } else {
            file.append(item);
        }
    }

    private void addItemWithNot(String item) {
        file.append("not (");
        file.append(item.substring(4));
        file.append(")");
    }

    private void addEnd() {
        file.append(")");
    }

    private String getTab() {
        return "    ";
    }

    private String getTwoTabs() {
        return "        ";
    }

    private String getEnd() {
        return "\n";
    }
}
