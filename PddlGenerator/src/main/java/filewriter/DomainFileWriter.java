package filewriter;

import model.ActionData;
import generators.PDDLInterface;
import utils.Constants;

import java.util.List;
import java.util.Map;

public class DomainFileWriter {

    private PDDLInterface pddlInterface;
    private StringBuilder file = new StringBuilder();

    public DomainFileWriter(PDDLInterface pddlInterface, List<ActionData> actionDataList) throws Exception {
        this.pddlInterface = pddlInterface;

        init(actionDataList);
    }

    private void init(List<ActionData> actionDataList) throws Exception {
        addStart(pddlInterface.getDomainFileName());
        addTypes(pddlInterface.getTypes());
        addPredicates(pddlInterface.getPredicates());
        addActions(actionDataList);
        addEnd();

        pddlInterface.setDomainFile(file.toString());
    }

    private void addStart(String domainFileName) {
        file.append("(define (domain ").append(domainFileName).append(")").append(getEnd());
        file.append(getTab());
        file.append("(:requirements :strips :typing :negative-preconditions :disjunctive-preconditions :conditional-effects)").append(getEnd());
        file.append(getEnd());
    }

    private void addTypes(Map<String, List<String>> types) {
        file.append(getTab());
        file.append("(:types");

        boolean firstRow = true;

        for (String key : types.keySet()) {
            if (firstRow) {
                firstRow = false;
            } else {
                file.append("           ");
            }

            for (String value : types.get(key)) {
                file.append(" ").append(value);
            }

            file.append(" - ").append(key).append(getEnd());
        }

        file.append(getTab()).append(")").append(getEnd());
        file.append(getEnd());
    }

    private void addPredicates(List<String> predicates) {
        file.append(getTab());
        file.append("(:predicates").append(getEnd());

        for (String predicate : predicates) {
            file.append(getTwoTabs());
            file.append("(").append(predicate).append(")\n");
        }

        file.append(getTab()).append(")").append(getEnd());
        file.append(getEnd());
    }

    private void addActions(List<ActionData> actionDataList) {
        for (ActionData actionData: actionDataList) {
            addAction(actionData);
            file.append(getEnd());
        }
    }

    private void addAction(ActionData actionData) {
        file.append(getTab());
        file.append("(:action ").append(actionData.getName()).append(getEnd());

        addParameters(actionData.getParameters());
        addPreconditions(actionData.getPreconditions());
        addEffects(actionData.getEffects());

        file.append(getTab());
        file.append(")").append(getEnd());
    }

    private void addParameters(List<String> parameters) {
        file.append(getTwoTabs());
        file.append(":parameters (");

        for (String parameter : parameters)
            file.append(parameter).append(" ");

        if (parameters.size() > 0)
            file.deleteCharAt(file.length() - 1);

        file.append(")").append(getEnd());
    }

    private void addPreconditions(List<String> preconditions) {
        file.append(getTwoTabs());
        file.append(":precondition (");
        addActionItems(preconditions);
        file.append(")").append(getEnd());
    }

    private void addEffects(List<String> effects) {
        file.append(getTwoTabs());
        file.append(":effect (");
        addActionItems(effects);
        file.append(")").append(getEnd());
    }

    private void addActionItems(List<String> itemList) {
        if (itemList.size() > 1) {
            addActionItemsWithAnd(itemList);
        } else if (itemList.size() > 0) {
            addActionItem(itemList.get(0));
        }
    }

    private void addActionItemsWithAnd(List<String> itemList) {
        file.append("and ");

        for (String item : itemList) {
            file.append("(");
            addActionItem(item);
            file.append(") ");
        }

        file.deleteCharAt(file.length() - 1);
    }

    private void addActionItem(String item) {
        String[] itemArray = item.split(" ");

        if (itemArray[0].equals(Constants.PARAMETER_NOT)) {
            addActionItemWithNot(item);
        } else {
            file.append(item);
        }
    }

    private void addActionItemWithNot(String item) {
        file.append("not (");
        file.append(item.substring(4));
        file.append(")");
    }

    private void addEnd() {
        file.append(")");
    }

    private String getTab() {
        return "    ";
    }

    private String getTwoTabs() {
        return "        ";
    }

    private String getEnd() {
        return "\n";
    }
}
