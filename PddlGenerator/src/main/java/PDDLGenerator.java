import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import generators.ActionFilter;
import generators.DomainGenerator;
import generators.PDDLInterface;
import generators.ProblemGenerator;
import model.PhoneData;
import model.UserData;
import model.database.Action;
import model.database.DatabaseData;
import model.database.Location;
import filewriter.DomainFileWriter;
import filewriter.ProblemFileWriter;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;

import static utils.Constants.TYPE_OBJECT;

public class PDDLGenerator implements PDDLInterface {

    private String domainFileName;
    private String problemFileName;

    private String databaseDataFile;

    private DatabaseData databaseData;

    private UserData userData;
    private PhoneData phoneData;

    private Map<String, List<String>> domainTypes = new HashMap<>();
    private Map<String, List<String>> problemObjects = new HashMap<>();
    private Map<String, List<Action>> locationActionList = new HashMap<>();

    private List<String> domainPredicates = new ArrayList<>();
    private List<String> problemInit = new ArrayList<>();
    private List<String> problemGoal = new ArrayList<>();

    private DomainGenerator domainGenerator;

    public PDDLGenerator(String databaseDataFile, String domainFileName, String problemFileName) throws Exception {
        this.databaseDataFile = databaseDataFile;

        this.domainFileName = domainFileName;
        this.problemFileName = problemFileName;

        init();
    }

    private void init() throws Exception {
        getUserData();
        getPhoneData();
        getSmartCityData();

        filterActionsBasedOnPhoneData();
    }

    private void getUserData() throws FileNotFoundException {
        Gson gson = new Gson();
        JsonReader jsonReader = new JsonReader(new FileReader("user_data.json"));
        userData = gson.fromJson(jsonReader, UserData.class);
    }

    private void getPhoneData() throws FileNotFoundException {
        Gson gson = new Gson();
        JsonReader jsonReader = new JsonReader(new FileReader("smart_phone_data.json"));
        phoneData = gson.fromJson(jsonReader, PhoneData.class);
    }

    private void getSmartCityData() throws FileNotFoundException {
        getDatabaseData();
    }

    private void getDatabaseData() throws FileNotFoundException {
        Gson gson = new Gson();
        JsonReader jsonReader = new JsonReader(new FileReader(databaseDataFile));
        databaseData = gson.fromJson(jsonReader, DatabaseData.class);
    }

    private void filterActionsBasedOnPhoneData() throws Exception {
        new ActionFilter(this, phoneData, databaseData.getActions());
    }

    private void writeFile(String fileName, String file) throws Exception {
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName + ".pddl"));
        if (file != null) {
            writer.write(file);
        } else {
            throw new Exception("Error while writing to file: " + fileName);
        }
        writer.close();
    }

    @Override
    public void setFilteredActions(List<Action> filteredActions) throws Exception {
        databaseData.setFilteredActions(filteredActions);

        // validateData();

        generatePDDLActionsAndProblems();
    }

    private void validateData() throws Exception {
        // int nrOfAvailableLocations = getNrOfAvailableLocations();
//
//        if (userData.getNrOfSightseeings() != 0 && userData.getNrOfSightseeings() > nrOfAvailableLocations) {
//            throw new Exception("Number of locations to do is smaller than list of available locations");
//        }
    }

//    private int getNrOfAvailableLocations() {
//        List<Integer> locationList = new ArrayList<>();
//
//        for (Action action : databaseData.getFilteredActions()) {
//            int locationId = action.getLocationId();
//            if (locationId != Action.NO_LOCATION_SET) {
//                if (!locationList.contains(locationId)) {
//                    locationList.add(locationId);
//                }
//            }
//        }
//
//        return locationList.size();
//    }

    private void generatePDDLActionsAndProblems() throws Exception {
        domainGenerator = new DomainGenerator(this, databaseData.getFilteredActions(), databaseData.getActionTypes(), databaseData.getHelpers());
        new ProblemGenerator(this, databaseData);

        createDomainAndProblemFiles();
    }

    private void createDomainAndProblemFiles() throws Exception {
        new DomainFileWriter(this, domainGenerator.getActionDataList());
        new ProblemFileWriter(this);
    }

    @Override
    public String getDomainFileName() {
        return domainFileName;
    }

    @Override
    public String getProblemFileName() {
        return problemFileName;
    }

    @Override
    public Map<String, List<String>> getObjects() {
        return problemObjects;
    }

    @Override
    public Map<String, List<String>> getTypes() {
        return domainTypes;
    }

    @Override
    public Map<String, List<Action>> getLocationActionList() {
        return locationActionList;
    }

    @Override
    public List<String> getPredicates() {
        return domainPredicates;
    }

    @Override
    public List<String> getInit() {
        return problemInit;
    }

    @Override
    public List<String> getGoals() {
        return problemGoal;
    }

    @Override
    public int getNrOfLocationsToDo() {
        return userData.getNrOfSightseeings();
    }

    @Override
    public Map<Integer, Integer> getLocationActionCountList() {
        return domainGenerator.getLocationActionCountList();
    }

    @Override
    public String getCurrentLocation() {
        return phoneData.getCurrentLocation().getName();
    }

    @Override
    public void addTypeObject(String type) {
        addType(TYPE_OBJECT, type);
    }

    @Override
    public void addType(String key, String type) {
        if (!domainTypes.containsKey(key)) {
            domainTypes.put(key, new ArrayList<>());
        }

        List<String> typeList = domainTypes.get(key);

        if (!typeList.contains(type)) {
            typeList.add(type);
            domainTypes.put(key, typeList);
        }
    }

    @Override
    public void addPredicate(String predicate) {
        if (!domainPredicates.contains(predicate)) {
            domainPredicates.add(predicate);
        }
    }

    @Override
    public void addObject(String key, String name) {
        if (!problemObjects.containsKey(key)) {
            problemObjects.put(key, new ArrayList<>());
        }

        List<String> objectList = problemObjects.get(key);

        if(!objectList.contains(name)) {
            objectList.add(name);
            problemObjects.put(key, objectList);
        }
    }

    @Override
    public void addInit(String init) {
        if (!problemInit.contains(init)) {
            problemInit.add(init);
        }
    }

    @Override
    public void addGoal(String goal) {
        if (!problemGoal.contains(goal)) {
            problemGoal.add(goal);
        }
    }

    @Override
    public void addLocationAction(int locationId, Action action) {
        String locationName = getLocationName(locationId);

        if (locationName != null) {
            if (!locationActionList.containsKey(locationName)) {
                locationActionList.put(locationName, new ArrayList<>());
            }

            List<Action> actionList = locationActionList.get(locationName);

            if (!actionList.contains(action)) {
                actionList.add(action);
                locationActionList.put(locationName, actionList);
            }
        }
    }

    @Override
    public String getLocationName(int locationId) {
        for (Location location: databaseData.getLocations()) {
            if (location.getId() == locationId) {
                return location.getName();
            }
        }
        return null;
    }

    @Override
    public void setDomainFile(String domainFileText) throws Exception {
        writeFile(domainFileName, domainFileText);
    }

    @Override
    public void setProblemFile(String problemFileText) throws Exception {
        writeFile(problemFileName, problemFileText);
    }
}
