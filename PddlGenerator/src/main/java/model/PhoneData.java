package model;

import model.database.Location;

import java.util.Map;

public class PhoneData {

    private Map<String, Boolean> phoneSpecifications;
    private Location currentLocation;

    public Map<String, Boolean> getPhoneSpecifications() {
        return phoneSpecifications;
    }

    public Location getCurrentLocation() {
        return currentLocation;
    }
}
