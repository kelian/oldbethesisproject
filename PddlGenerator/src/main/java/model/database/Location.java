package model.database;

public class Location {
    private int id = -1;
    private String name;
    private String methodName = null;
    private float lat;
    private float lng;

    public int getId() {
        return id;
    }

    public String getName() {
        if (methodName == null && name != null) {
            methodName = name
                    .replaceAll(" ", "_")
                    .replaceAll("'","_");
        }
        return methodName;
    }

}
