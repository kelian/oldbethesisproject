package model.database;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Action {

    public static final int NO_LOCATION_SET = -1;

    private String name;
    private List<String> preconditions = new ArrayList<>();
    private List<String> effects = new ArrayList<>();
    private List<String> init = new ArrayList<>();

    private String bpmn = null;

    private String type = null;

    private Boolean goal = false;
    private Boolean locationCompulsoryAction = false;

    private Map<String, Boolean> specifications;
    private Map<String, List<String>> helpers;

    public void addPreconditions(List<String> preconditions) {
        this.preconditions.addAll(preconditions);
    }

    public void addEffects(List<String> effects) {
        this.effects.addAll(effects);
    }

    public String getName() {
        return name;
    }

    public String getBpmn() {
        return bpmn;
    }

    public String getType() {
        return type;
    }

    public List<String> getPreconditions() {
        return preconditions;
    }

    public List<String> getEffects() {
        return effects;
    }

    public List<String> getInit() {
        return init;
    }

    public Map<String, Boolean> getSpecifications() {
        return specifications;
    }

    public Map<String, List<String>> getHelpers() {
        return helpers;
    }

    public Boolean isLocationCompulsoryAction() {
        return locationCompulsoryAction;
    }

    public Boolean isGoal() {
        return goal;
    }

    public Boolean hasType() {
        return type != null && !type.isEmpty();
    }

    public Boolean hasHelpers() {
        return helpers != null && !helpers.isEmpty();
    }
}
