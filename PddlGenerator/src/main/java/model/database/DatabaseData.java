package model.database;

import java.util.List;

public class DatabaseData {
    private Problem problem;
    private List<Action> actions;
    private List<Helper> helpers;
    private List<Action> types;
    private List<Location> locations;

    private List<Action> filteredActions;

    public List<Action> getActions() {
        return actions;
    }

    public List<Action> getActionTypes() {
        return types;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public Problem getProblem() {
        return problem;
    }

    public List<Helper> getHelpers() {
        return helpers;
    }

    public void setFilteredActions(List<Action> actions) {
        this.filteredActions = actions;
    }

    public List<Action> getFilteredActions() {
        return filteredActions;
    }
}
