package model.database;

import java.util.List;

public class Helper {

    private String name;
    private List<String> preconditions;
    private List<String> effects;
    private List<Action> actions;

    public String getName() {
        return name;
    }

    public List<String> getPreconditions() {
        return preconditions;
    }

    public List<String> getEffects() {
        return effects;
    }

    public List<Action> getActions() {
        return actions;
    }
}
