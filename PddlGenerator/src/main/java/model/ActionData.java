package model;

import java.util.ArrayList;
import java.util.List;

public class ActionData {

    private String name;
    private List<String> parameters = new ArrayList<>();
    private List<String> preconditions = new ArrayList<>();
    private List<String> effects = new ArrayList<>();

    public ActionData(String name) {
        this.name = name;
    }

    public ActionData(String name, List<String> parameters, List<String> preconditions, List<String> effects) {
        this.name = name;
        this.parameters = parameters;
        this.preconditions = preconditions;
        this.effects = effects;
    }

    public void addParameter(String parameter) {
        String[] parameterSplit = parameter.split("-");

        boolean hasParameter = false;
        for (String listParameter: parameters) {
            if (listParameter.startsWith(parameterSplit[0])) {
                hasParameter = true;
            }
        }

        if (!hasParameter) {
            parameters.add(parameter);
        }
    }

    public void addPrecondition(String precondition) {
        preconditions.add(precondition);
    }

    public void addEffect(String effect) {
        effects.add(effect);
    }

    public void setPreconditions(List<String> preconditions) {
        this.preconditions = preconditions;
    }

    public void setEffects(List<String> effects) {
        this.effects = effects;
    }

    public String getName() {
        return name;
    }

    public List<String> getParameters() {
        return parameters;
    }

    public List<String> getPreconditions() {
        return preconditions;
    }

    public List<String> getEffects() {
        return effects;
    }
}
