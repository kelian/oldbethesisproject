package model;

import model.database.Location;

public class UserData {

    private int nrOfSightseeings;
    private Location location;

    public int getNrOfSightseeings() {
        return nrOfSightseeings;
    }

    public Location getLocation() {
        return location;
    }
}
