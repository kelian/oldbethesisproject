package utils;

public class Constants {

    public static final String TYPE_OBJECT = "object";
    public static final String TYPE_LOCATION = "location";
    public static final String TYPE_GOAL = "goal";

    public static final String PARAMETER_NOT = "not";
    public static final String PARAMETER_ACTION_DONE = "is_action_done";
    public static final String PARAMETER_SEQUENTIAL = "_in_progress";
    public static final String PARAMETER_LOCATION_DONE = "is_location_done";
    public static final String PARAMETER_HAS_ACTIONS = "has_actions";
    public static final String PARAMETER_GOAL_DONE = "is_goal_done";
    public static final String PARAMETER_GOAL_LOCATION_DONE = "is_location_goal_done";

}
